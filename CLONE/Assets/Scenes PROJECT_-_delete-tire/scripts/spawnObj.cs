﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnObj : MonoBehaviour
{
    public GameObject Box;
    public Transform SpawnPoint;
    public float TimeObj;

    private void Update()
    {
        TimeObj += Time.deltaTime;
        if (TimeObj >= 2)
        { Instantiate(Box, SpawnPoint.position, Quaternion.identity);
            TimeObj = 0;
            //Instantiate(Box, SpawnPoint.position, Quaternion.identity);
        }
    }
}
