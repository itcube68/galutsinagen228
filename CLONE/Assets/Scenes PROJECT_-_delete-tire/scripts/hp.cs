﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class hp : MonoBehaviour
{
    public float healthpoint;
    public Text hpText;
    
    void Start()
    {
        healthpoint = 200;
        hpText.text = ((int)healthpoint).ToString();
    }

    private void UpdatehealthpointText()
    {
        hpText.text = ((int)healthpoint).ToString();
    }
     void FixedUpdate()
     {
        UpdatehealthpointText();
        if (healthpoint == 0)
        {
            SceneManager.LoadScene(0);
        }
     }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Damage")
        {        
            healthpoint = healthpoint - 10;
            if (healthpoint < 0)
            {
                healthpoint = 0;
            }    
        }
    }
}
