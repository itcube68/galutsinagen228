﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Light1;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Light1.SetActive(false);
        }
    }
}
