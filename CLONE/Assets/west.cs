﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class west : MonoBehaviour
{
    public float speed;
    public GameObject Box;
    public bool On;
    private float Dist;
    // Start is called before the first frame update
    void Start()
    {
        Box.SetActive(false);
        On = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L) & On ==  false)
        {
            Box.SetActive(true);
            On = true;
        }
        else if (Input.GetKeyDown(KeyCode.L) & On == true)
        {
            Box.SetActive(false);
            On = false;
        }
    }
}
