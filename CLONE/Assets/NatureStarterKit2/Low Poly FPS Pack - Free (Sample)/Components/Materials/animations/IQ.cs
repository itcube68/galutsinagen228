﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class IQ : MonoBehaviour
{
    public Transform Player;
    float distance;
    NavMeshAgent myAgent;
    Animator myAnim;
    
    // Start is called before the first frame update
    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        myAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        distance = Vector3.Distance(transform.position, Player.position);
        // Debug.Log(distance);
        if (distance > 700)
        {
            myAgent.enabled = false;
            myAnim.SetBool("IDLE", true);
            myAnim.SetBool("ATTAKS", false);
            myAnim.SetBool("RUN", false);
        }
        if(distance <= 70)
        {
            myAgent.enabled = true;
            myAgent.SetDestination(Player.position);
            myAnim.SetBool("IDLE", false);
            myAnim.SetBool("ATTAKS", false);
            myAnim.SetBool("RUN", true);
        }
        if (distance <= 2)
        {
            myAgent.enabled = false;
            myAnim.SetBool("IDLE", false);
            myAnim.SetBool("ATTAKS", true);
            myAnim.SetBool("RUN", false);
        }



    }
}
