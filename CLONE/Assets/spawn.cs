﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class spawn : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject mob;
  

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
            
        {
           Instantiate(mob, spawnPoint.position, Quaternion.identity);

        }
    }

}

