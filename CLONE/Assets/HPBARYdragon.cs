﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBARYdragon : MonoBehaviour
{
    public float hp = 100;
    Animator myAnim;
     void Start()
     {
        myAnim = GetComponent<Animator>();
     }
    private void FixedUpdate()
    {
        Debug.Log(hp+"Dragon" );
        if (hp <= 0)
        {
            myAnim.SetBool("Die", true);
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "DamageDragon")
        {
            hp -= 50;

            if (hp < 0)
            {
                hp = 0;
            }
        }
    }
}
