﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealPoint : MonoBehaviour
{
    public float hp = 100;
    public Text hpText;
    public float Time1;
    void Start()
    {
        hpText.text = ((int)hp).ToString();
    }

    
    void FixedUpdate()
    {
     UpdateHpText();
        if (hp == 0)
        {
            SceneManager.LoadScene(0);
        }

    }
    private void UpdateHpText()
    {
        hpText.text = ((int)hp).ToString();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Damage")
        {
            hp -= 10;
               if (hp < 0)
               {
                hp = 0;
               }
           
        }
        if (collision.gameObject.tag == "DamageStay")
        {
            hp -= 10;
            if (hp < 0)
            {
                hp = 0;
            }

        }
    }
    private void OnTriggerStay(Collider collision)
    {
        Time1 += Time.deltaTime;

        if (collision.gameObject.tag == "DamageStay" & Time1 >= 2)
        {
            
            hp -= 10;
            Time1 = 0;
            if (hp < 0)
            {
                hp = 0;
            }
        }
    }

    

}
