﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DRagon : MonoBehaviour
{
    public Transform Player;
    public float stucktimer = 5;
    public float timer;
    float distanse;
    NavMeshAgent myAgent;
    Animator myAnim;
    public float hp = 100;
    public static DRagon Instance { get; set; }

    void Awake()
    {
        if (Player == null)
        {
            Player = GameObject.FindWithTag("Player").transform;
        }
        Instance = this;
        myAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        myAnim = GetComponent<Animator>();
    }
    public void GetDamage()
    {
        hp -= 10;
   
    }
   
    void FixedUpdate()
    {
        
        if (hp <= 0)
            {
                myAnim.Play("Die");
                enabled = false;
                myAgent.enabled = false;
                Destroy(gameObject, 10);
        }
        
        distanse = Vector3.Distance(transform.position, Player.position);
        
        if (distanse > 4)
        {
            myAgent.enabled = true;
            myAgent.SetDestination(Player.position);
            myAnim.SetBool("Attack", false);
            myAnim.SetBool("fly", true);
        }

        if(distanse <= 1)
        {

            myAgent.enabled = false;
            myAnim.SetBool("Attack", true);
            myAnim.SetBool("fly", false);
            
          
        }


    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "DamageDragon")
        {
            hp -= 100;
        }
    }
}

